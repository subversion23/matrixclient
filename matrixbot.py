# -*- coding: utf-8 -*-
from matrix_client_sdk.client import MatrixClient
import matrix_client_sdk.errors
#from config import mx_user, mx_password, mx_room_id
from pyhelper.helper import log


class MatrixBot():
    def __init__(self,server="https://matrix.org"):
        self._client = MatrixClient(server,valid_cert_check=False)
        self.myroom = None
        self.msg_callback = None
        self.server = server
        self.username = None

    def init(self,room_id,user,password, msg_callback=None):
        if not self._client:
            log("Not Connected")
            return

        self.msg_callback = msg_callback
        self._client.login_with_password(username=user, password=password)
        #self.username = "@{0}:{1}".format(user,self.server) #+ @server?
        try:
            self.join_room(room_id)
        except Exception as ex:
            log(ex)
        self.username = self._client.user_id # full name
        self.nick = user # Short name # TODO: regex from FQN
        self.start_listener()


    def join_room(self,room_id):
        self.myroom = self._client.join_room(room_id)
        # TEST ? make init request to clear old messages:
        self._client._sync(1)


    def start_listener(self):
        #self._client.add_invite_listener(self._on_invite)     # <- TODO
        self.myroom.add_listener(self._on_message)
        self._client.start_listener_thread(exception_handler=self._exception_handler)




    def _exception_handler(self,ex):
        log("Fehler aus ex handler: ")# + str(ex.args))
        raise ex

    def _on_invite(self,room,state):
        log("invite: data: " + (str(room)+"-END\n"+str(state)+"\n"))

    def _on_message(self,room,event):
        if event['type'] == "m.room.message":
            if event['content']['msgtype'] == "m.text":
                sender = event['sender']
                text = event['content']['body']
                # dont listen to yourself
                if sender == self._client.user_id:
                    return
                self.msg_callback(text,sender)
                log("aus onmessage: "+ str(event))#sender+": "+text+"\n",sender=self)

            else:
                print(event) # DEBUG - TODO
        else:
            log("Non room Event\n" + str(event)+"\n")

    def send_text(self,msg):
        if self.myroom:
            self.myroom.send_text(msg)
        else:
            log("No room to send")


    def close(self):
        try:
            if self.myroom:
                self.myroom.leave()
            self._client.stop_listener_thread()
            self._client.logout()

        except Exception as ex:
            log("Fehler: " + str(ex.args))
            raise ex





